<?php
  use google\appengine\api\users\User;
  use google\appengine\api\users\UserService;
  $user = UserService::getCurrentUser();

  header("Content-Type: text/html; charset=UTF-8");

  if (!$user) {
  	header('Location: ' . UserService::createLoginURL($_SERVER['REQUEST_URI']));
  }
?>

<html>
 <body>
  <?php

  // Create a connection.
   $db = null;
   if (isset($_SERVER['SERVER_SOFTWARE']) &&
      strpos($_SERVER['SERVER_SOFTWARE'], 'Google App Engine') !== false) {
    // Connect from App Engine.
	try {
		$db = new pdo('mysql:unix_socket=/cloudsql/test-153209:ryuasobi;dbname=sotuken;charset=utf8', '3bji1113', '');
    	} catch (PDOException $ex) {
      		die('Unable to connect.');
    	}
   }
  ?>

  <h2>個人情報変更</h2>

    <form action="/phpselect">
      <div>
	<input type="submit" value="メニューへ戻る">
      </div>
    </form>

    <form>
      <div>どれか選択してください</div>
      <div>
	<select name="address">
	<option value="北海道">北海道</option>
	<option value="青森県">青森県</option>
	<option value="岩手県">岩手県</option>
	<option value="宮城県">宮城県</option>
	<option value="秋田県">秋田県</option>
	<option value="山形県">山形県</option>
	<option value="福島県">福島県</option>
	<option value="茨城県">茨城県</option>
	<option value="栃木県">栃木県</option>
	<option value="群馬県">群馬県</option>
	<option value="埼玉県">埼玉県</option>
	<option value="千葉県">千葉県</option>
	<option value="東京都">東京都</option>
	<option value="神奈川県">神奈川県</option>
	<option value="新潟県">新潟県</option>
	<option value="富山県">富山県</option>
	<option value="石川県">石川県</option>
	<option value="福井県">福井県</option>
	<option value="山梨県">山梨県</option>
	<option value="長野県">長野県</option>
	<option value="岐阜県">岐阜県</option>
	<option value="静岡県">静岡県</option>
	<option value="愛知県">愛知県</option>
	<option value="三重県">三重県</option>
	<option value="滋賀県">滋賀県</option>
	<option value="京都府">京都府</option>
	<option value="大阪府">大阪府</option>
	<option value="兵庫県">兵庫県</option>
	<option value="奈良県">奈良県</option>
	<option value="和歌山県">和歌山県</option>
	<option value="鳥取県">鳥取県</option>
	<option value="島根県">島根県</option>
	<option value="岡山県">岡山県</option>
	<option value="広島県">広島県</option>
	<option value="山口県">山口県</option>
	<option value="徳島県">徳島県</option>
	<option value="香川県">香川県</option>
	<option value="愛媛県">愛媛県</option>
	<option value="高知県">高知県</option>
	<option value="福岡県">福岡県</option>
	<option value="佐賀県">佐賀県</option>
	<option value="長崎県">長崎県</option>
	<option value="熊本県">熊本県</option>
	<option value="大分県">大分県</option>
	<option value="宮崎県">宮崎県</option>
	<option value="鹿児島県">鹿児島県</option>
	<option value="沖縄県">沖縄県</option>
	</select>
      </div>
      <div>
	<input type="submit" name="address1000" value="address1000">
      </div>
      <div>
	<input type="submit" name="address10000" value="address10000">
      </div>
      <div>
	<input type="submit" name="address100000" value="address100000">
      </div>
    </form>

  <?php
    $address = $_GET["address"];
    if(isset($_GET["address1000"])){
	echo "<br> update address 1000 <br>";
    	for($j = 0; $j < 10; $j++){
    		try {
      			$stmt = $db->prepare('update Japanese set Address = :Address order by rand() limit 1000');
      			$start = microtime(true);
      			$stmt->execute(array(':Address' => $address));
      			$end = microtime(true);
      			$time = $end - $start;
      			echo "<br>$time";
      			$affected_rows = $stmt->rowCount();
      			// Log $affected_rows. 
      		} catch (PDOException $ex) {
      			// Log error.
      			echo "<br>失敗";
      		}
	}
   }

    if(isset($_GET["address10000"])){
	echo "<br> update address 10000 <br>";
    	for($j = 0; $j < 10; $j++){
    		try {
      			$stmt = $db->prepare('update Japanese set Address = :Address order by rand() limit 10000');
      			$start = microtime(true);
      			$stmt->execute(array(':Address' => $address));
      			$end = microtime(true);
      			$time = $end - $start;
      			echo "<br>$time";
      			$affected_rows = $stmt->rowCount();
      			// Log $affected_rows. 
      		} catch (PDOException $ex) {
      			// Log error.
      			echo "<br>失敗";
      		}
    	}
    }

    if(isset($_GET["address100000"])){
	echo "<br> update address 100000 <br>";
    	for($j = 0; $j < 10; $j++){
    		try {
      			$stmt = $db->prepare('update Japanese set Address = :Address order by rand() limit 100000');
      			$start = microtime(true);
      			$stmt->execute(array(':Address' => $address));
      			$end = microtime(true);
      			$time = $end - $start;
      			echo "<br>$time";
      			$affected_rows = $stmt->rowCount();
      			// Log $affected_rows. 
      		} catch (PDOException $ex) {
      			// Log error.
      			echo "<br>失敗";
      		}
    	}
    }

  ?>
 </body>
</html>