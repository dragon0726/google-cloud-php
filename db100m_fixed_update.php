<?php
  use google\appengine\api\users\User;
  use google\appengine\api\users\UserService;
  $user = UserService::getCurrentUser();

  header("Content-Type: text/html; charset=UTF-8");

  if (!$user) {
  	header('Location: ' . UserService::createLoginURL($_SERVER['REQUEST_URI']));
  }
?>

<html>
 <body>
  <?php

  // Create a connection.
   $db = null;
   if (isset($_SERVER['SERVER_SOFTWARE']) &&
      strpos($_SERVER['SERVER_SOFTWARE'], 'Google App Engine') !== false) {
    // Connect from App Engine.
	try {
		$db = new pdo('mysql:unix_socket=/cloudsql/test2-3-331:mydb1;dbname=sotuken1;charset=utf8', '3bji1113', '');
    	} catch (PDOException $ex) {
      		die('Unable to connect.');
    	}
   }
  ?>

  <h2>個人情報変更</h2>

    <form action="/phpselect">
      <div>
	<input type="submit" value="メニューへ戻る">
      </div>
    </form>

    <form>
      <div>どれか選択してください</div>
      <div>
	<select name="age">
	<option value="0">0</option>
	<option value="1">1</option>
	<option value="2">2</option>
	<option value="3">3</option>
	<option value="4">4</option>
	<option value="5">5</option>
	<option value="6">6</option>
	<option value="7">7</option>
	<option value="8">8</option>
	<option value="9">9</option>
	<option value="10">10</option>
	<option value="11">11</option>
	<option value="12">12</option>
	<option value="13">13</option>
	<option value="14">14</option>
	<option value="15">15</option>
	<option value="16">16</option>
	<option value="17">17</option>
	<option value="18">18</option>
	<option value="19">19</option>
	<option value="20">20</option>
	<option value="21">21</option>
	<option value="22">22</option>
	<option value="23">23</option>
	<option value="24">24</option>
	<option value="25">25</option>
	<option value="26">26</option>
	<option value="27">27</option>
	<option value="28">28</option>
	<option value="29">29</option>
	<option value="30">30</option>
	<option value="31">31</option>
	<option value="32">32</option>
	<option value="33">33</option>
	<option value="34">34</option>
	<option value="35">35</option>
	<option value="36">36</option>
	<option value="37">37</option>
	<option value="38">38</option>
	<option value="39">39</option>
	<option value="40">40</option>
	<option value="41">41</option>
	<option value="42">42</option>
	<option value="43">43</option>
	<option value="44">44</option>
	<option value="45">45</option>
	<option value="46">46</option>
	<option value="47">47</option>
	<option value="48">48</option>
	<option value="49">49</option>
	<option value="50">50</option>
	<option value="51">51</option>
	<option value="52">52</option>
	<option value="53">53</option>
	<option value="54">54</option>
	<option value="55">55</option>
	<option value="56">56</option>
	<option value="57">57</option>
	<option value="58">58</option>
	<option value="59">59</option>
	<option value="60">60</option>
	<option value="61">61</option>
	<option value="62">62</option>
	<option value="63">63</option>
	<option value="64">64</option>
	<option value="65">65</option>
	<option value="66">66</option>
	<option value="67">67</option>
	<option value="68">68</option>
	<option value="69">69</option>
	<option value="70">70</option>
	<option value="71">71</option>
	<option value="72">72</option>
	<option value="73">73</option>
	<option value="74">74</option>
	<option value="75">75</option>
	<option value="76">76</option>
	<option value="77">77</option>
	<option value="78">78</option>
	<option value="79">79</option>
	<option value="80">80</option>
	<option value="81">81</option>
	<option value="82">82</option>
	<option value="83">83</option>
	<option value="84">84</option>
	<option value="85">85</option>
	<option value="86">86</option>
	<option value="87">87</option>
	<option value="88">88</option>
	<option value="89">89</option>
	<option value="90">90</option>
	<option value="91">91</option>
	<option value="92">92</option>
	<option value="93">93</option>
	<option value="94">94</option>
	<option value="95">95</option>
	<option value="96">96</option>
	<option value="97">97</option>
	<option value="98">98</option>
	<option value="99">99</option>
	<option value="100">100</option>
	<option value="101">101</option>
	<option value="102">102</option>
	<option value="103">103</option>
	<option value="104">104</option>
	<option value="105">105</option>
	</select>

	<select name="address">
	<option value="北海道">北海道</option>
	<option value="青森県">青森県</option>
	<option value="岩手県">岩手県</option>
	<option value="宮城県">宮城県</option>
	<option value="秋田県">秋田県</option>
	<option value="山形県">山形県</option>
	<option value="福島県">福島県</option>
	<option value="茨城県">茨城県</option>
	<option value="栃木県">栃木県</option>
	<option value="群馬県">群馬県</option>
	<option value="埼玉県">埼玉県</option>
	<option value="千葉県">千葉県</option>
	<option value="東京都">東京都</option>
	<option value="神奈川県">神奈川県</option>
	<option value="新潟県">新潟県</option>
	<option value="富山県">富山県</option>
	<option value="石川県">石川県</option>
	<option value="福井県">福井県</option>
	<option value="山梨県">山梨県</option>
	<option value="長野県">長野県</option>
	<option value="岐阜県">岐阜県</option>
	<option value="静岡県">静岡県</option>
	<option value="愛知県">愛知県</option>
	<option value="三重県">三重県</option>
	<option value="滋賀県">滋賀県</option>
	<option value="京都府">京都府</option>
	<option value="大阪府">大阪府</option>
	<option value="兵庫県">兵庫県</option>
	<option value="奈良県">奈良県</option>
	<option value="和歌山県">和歌山県</option>
	<option value="鳥取県">鳥取県</option>
	<option value="島根県">島根県</option>
	<option value="岡山県">岡山県</option>
	<option value="広島県">広島県</option>
	<option value="山口県">山口県</option>
	<option value="徳島県">徳島県</option>
	<option value="香川県">香川県</option>
	<option value="愛媛県">愛媛県</option>
	<option value="高知県">高知県</option>
	<option value="福岡県">福岡県</option>
	<option value="佐賀県">佐賀県</option>
	<option value="長崎県">長崎県</option>
	<option value="熊本県">熊本県</option>
	<option value="大分県">大分県</option>
	<option value="宮崎県">宮崎県</option>
	<option value="鹿児島県">鹿児島県</option>
	<option value="沖縄県">沖縄県</option>
	</select>
      </div>
      <div>
	<input type="submit" name="age1000" value="age1000">
      </div>
      <div>
	<input type="submit" name="age10000" value="age10000">
      </div>
      <div>
	<input type="submit" name="age10920" value="age10920">
      </div>
      <div>
	<input type="submit" name="age100000" value="age100000">
      </div>
      <div>
	<input type="submit" name="address1000" value="address1000">
      </div>
      <div>
	<input type="submit" name="address10000" value="address10000">
      </div>
      <div>
	<input type="submit" name="address100000" value="address100000">
      </div>
    </form>

  <?php

   $age = $_GET["age"];
   $address = $_GET["address"];

   if(isset($_GET["age1000"])){
	echo "<br> update age 1000 <br>";
	for($i = 0; $i < 10; $i++){
    		try {
    			$stmt = $db->prepare('update Japanese1 set Age = :Age where ID > :offset limit 1000');
    			$start = microtime(true);
				$offset = $i * 1000;
				$stmt->bindValue(':Age', $age, PDO::PARAM_INT);
				$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
    			$end = microtime(true);
    			$time = $end - $start;
    			echo "<br>$time";
    			$affected_rows = $stmt->rowCount();
    		// Log $affected_rows. 
    		} catch (PDOException $ex) {
    			// Log error.
    			echo "<br>失敗";
    		}
	}
   }

   if(isset($_GET["age10000"])){
	echo "<br> update age 10000 <br>";
	for($i = 0; $i < 10; $i++){
    		try {
    			$stmt = $db->prepare('update Japanese1 set Age = :Age where ID > :offset limit 10000');
    			$start = microtime(true);
			    $offset = $i * 10000;
				$stmt->bindValue(':Age', $age, PDO::PARAM_INT);
				$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
    			$end = microtime(true);
    			$time = $end - $start;
    			echo "<br>$time";
    			$affected_rows = $stmt->rowCount();
    		// Log $affected_rows. 
    		} catch (PDOException $ex) {
    			// Log error.
    			echo "<br>失敗";
    		}
	}
   }
   
   if(isset($_GET["age10920"])){
	echo "<br> update age 10000 <br>";
	for($i = 0; $i < 10; $i++){
    		try {
    			$stmt = $db->prepare('update Japanese1 set Age = :Age where ID > :offset limit 10920');
    			$start = microtime(true);
			    $offset = $i * 10920;
				$stmt->bindValue(':Age', $age, PDO::PARAM_INT);
				$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
    			$end = microtime(true);
    			$time = $end - $start;
    			echo "<br>$time";
    			$affected_rows = $stmt->rowCount();
    		// Log $affected_rows. 
    		} catch (PDOException $ex) {
    			// Log error.
    			echo "<br>失敗";
    		}
	}
   }
   
   if(isset($_GET["age100000"])){
	echo "<br> update age 100000 <br>";
	for($i = 0; $i < 10; $i++){
    		try {
    			$stmt = $db->prepare('update Japanese1 set Age = :Age where ID > :offset limit 100000');
    			$start = microtime(true);
			    $offset = $i * 100000;
				$stmt->bindValue(':Age', $age, PDO::PARAM_INT);
				$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
    			$end = microtime(true);
    			$time = $end - $start;
    			echo "<br>$time";
    			$affected_rows = $stmt->rowCount();
    		// Log $affected_rows. 
    		} catch (PDOException $ex) {
    			// Log error.
    			echo "<br>失敗";
    		}
	}
   }

   if(isset($_GET["address1000"])){
	echo "<br> update address 1000 <br>";
	for($i = 0; $i < 10; $i++){
		try {
      			$stmt = $db->prepare('update Japanese1 set Address = :Address where ID > :offset limit 1000');
      			$start = microtime(true);
			    $offset = $i * 1000;
      			$stmt->bindValue(':Address', $address, PDO::PARAM_STR);
				$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
      			$end = microtime(true);
      			$time = $end - $start;
      			echo "<br>$time";
      			$affected_rows = $stmt->rowCount();
      			// Log $affected_rows. 
      		} catch (PDOException $ex) {
      			// Log error.
      			echo "<br>失敗";
      		}
	}
    }

   if(isset($_GET["address10000"])){
	echo "<br> update address 10000 <br>";
	for($i = 0; $i < 10; $i++){
		try {
      			$stmt = $db->prepare('update Japanese1 set Address = :Address where ID > :offset limit 10000');
      			$start = microtime(true);
			    $offset = $i * 10000;
      			$stmt->bindValue(':Address', $address, PDO::PARAM_STR);
				$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
      			$end = microtime(true);
      			$time = $end - $start;
      			echo "<br>$time";
      			$affected_rows = $stmt->rowCount();
      			// Log $affected_rows. 
      		} catch (PDOException $ex) {
      			// Log error.
      			echo "<br>失敗";
      		}
	}
    }

   if(isset($_GET["address100000"])){
	echo "<br> update address 100000 <br>";
	for($i = 0; $i < 10; $i++){
		try {
      			$stmt = $db->prepare('update Japanese1 set Address = :Address where ID > :offset limit 100000');
      			$start = microtime(true);
			    $offset = $i * 100000;
      			$stmt->bindValue(':Address', $address, PDO::PARAM_STR);
				$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
      			$end = microtime(true);
      			$time = $end - $start;
      			echo "<br>$time";
      			$affected_rows = $stmt->rowCount();
      			// Log $affected_rows. 
      		} catch (PDOException $ex) {
      			// Log error.
      			echo "<br>失敗";
      		}
	}
    }

  ?>
 </body>
</html>