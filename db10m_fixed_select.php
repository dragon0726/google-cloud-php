<?php
  use google\appengine\api\users\User;
  use google\appengine\api\users\UserService;
  $user = UserService::getCurrentUser();

  header("Content-Type: text/html; charset=UTF-8");

  if (!$user) {
  	header('Location: ' . UserService::createLoginURL($_SERVER['REQUEST_URI']));
  }
?>

<html>
 <body>
  <?php

  // Create a connection.
   $db = null;
   if (isset($_SERVER['SERVER_SOFTWARE']) &&
      strpos($_SERVER['SERVER_SOFTWARE'], 'Google App Engine') !== false) {
    // Connect from App Engine.
	try {
		$db = new pdo('mysql:unix_socket=/cloudsql/test2-3-331:mydb1;dbname=sotuken1;charset=utf8', '3bji1113', '');
    	} catch (PDOException $ex) {
      		die('Unable to connect.');
    	}
   }
  ?>

 <h2>個人情報出力</h2>

    <form action="/phpselect">
      <div>
	<input type="submit" value="メニューへ戻る">
      </div>
    </form>

 <div>カラム指定なし</div>
  <form>
  <div><input type="submit" name="run1" value="実行* 1000"></div>
  <div><input type="submit" name="run2" value="実行10000"></div>
  <div><input type="submit" name="run4" value="実行int 1000"></div>
  <div><input type="submit" name="run5" value="実行10000"></div>
  <div><input type="submit" name="run7" value="実行char 1000"></div>
  <div><input type="submit" name="run8" value="実行10000"></div>
  <div><input type="submit" name="run10" value="実行all 1000"></div>
  <div><input type="submit" name="run11" value="実行10000"></div>
  </form>
  <?php
   if(isset($_GET["run1"])){
		echo "<br> カラム指定なし(1000件) <br>";
		$stmt = $db->prepare('select * from Japanese1 limit :offset,1000');
		for($i = 0; $i < 10; $i++){
        		$start = microtime(true);
			    $offset = $i * 1000;
                $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
       			$end = microtime(true);
        		$time = $end - $start;
        		echo "<br>$time";
        		$affected_rows = $stmt->rowCount();
		}
		echo "<br>";
   }
   
   if(isset($_GET["run2"])){
		echo "<br> カラム指定なし(10000件) <br>";
		$stmt = $db->prepare('select * from Japanese1 limit :offset,10000');
		for($i = 0; $i < 10; $i++){
        		$start = microtime(true);
			    $offset = $i * 10000;
                $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
       			$end = microtime(true);
        		$time = $end - $start;
        		echo "<br>$time";
        		$affected_rows = $stmt->rowCount();
		}
		echo "<br>";
   }

   if(isset($_GET["run4"])){
		echo "<br> カラム１指定int(1000件) <br>";
		$stmt = $db->prepare('select ID from Japanese1 limit :offset,1000');
		for($i = 0; $i < 10; $i++){
        		$start = microtime(true);
                $offset = $i * 1000;
			    $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
       			$end = microtime(true);
        		$time = $end - $start;
        		echo "<br>$time";
        		$affected_rows = $stmt->rowCount();
		}
		echo "<br>";
   } 

   if(isset($_GET["run5"])){
		echo "<br> カラム１指定int(10000件) <br>";
		$stmt = $db->prepare('select ID from Japanese1 limit :offset,10000');
		for($i = 0; $i < 10; $i++){
        		$start = microtime(true);
                $offset = $i * 10000;
			    $stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
       			$end = microtime(true);
        		$time = $end - $start;
        		echo "<br>$time";
        		$affected_rows = $stmt->rowCount();
		}
		echo "<br>";
   } 

   
   if(isset($_GET["run7"])){
		echo "<br> カラム１指定char(1000件) <br>";
		$stmt = $db->prepare('select LastName from Japanese1 limit :offset,1000');
		for($i = 0; $i < 10; $i++){
        		$start = microtime(true);
			    $offset = $i * 1000;
        		$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
       			$end = microtime(true);
        		$time = $end - $start;
        		echo "<br>$time";
        		$affected_rows = $stmt->rowCount();
		}
		echo "<br>";
   }

   if(isset($_GET["run8"])){
		echo "<br> カラム１指定char(10000件) <br>";
		$stmt = $db->prepare('select LastName from Japanese1 limit :offset,10000');
		for($i = 0; $i < 10; $i++){
        		$start = microtime(true);
			    $offset = $i * 10000;
        		$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
       			$end = microtime(true);
        		$time = $end - $start;
        		echo "<br>$time";
        		$affected_rows = $stmt->rowCount();
		}
		echo "<br>";
   }

   if(isset($_GET["run10"])){
		echo "<br> カラム全部指定(1000件) <br>";
        	$stmt = $db->prepare('select ID,LastName,FirstName,LastName_kana,FirstName_kana,LastName_roma,FirstName_roma,Age,Address from Japanese1 limit :offset,1000');
		for($i = 0; $i < 10; $i++){
        		$start = microtime(true);
			    $offset = $i * 1000;
        		$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
       			$end = microtime(true);
        		$time = $end - $start;
        		echo "<br>$time";
        		$affected_rows = $stmt->rowCount();
		}
		echo "<br>";
   }

   if(isset($_GET["run11"])){
		echo "<br> カラム全部指定(10000件) <br>";
        	$stmt = $db->prepare('select ID,LastName,FirstName,LastName_kana,FirstName_kana,LastName_roma,FirstName_roma,Age,Address from Japanese1 limit :offset,10000');
		for($i = 0; $i < 10; $i++){
        		$start = microtime(true);
			    $offset = $i * 10000;
        		$stmt->bindValue(':offset', $offset, PDO::PARAM_INT);
        		$stmt->execute();
       			$end = microtime(true);
        		$time = $end - $start;
        		echo "<br>$time";
        		$affected_rows = $stmt->rowCount();
		}
		echo "<br>";
   }

?>

 </body>
</html>