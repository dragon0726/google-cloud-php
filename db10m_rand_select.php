<?php
  use google\appengine\api\users\User;
  use google\appengine\api\users\UserService;
  $user = UserService::getCurrentUser();

  header("Content-Type: text/html; charset=UTF-8");

  if (!$user) {
  	header('Location: ' . UserService::createLoginURL($_SERVER['REQUEST_URI']));
  }
?>

<html>
 <body>
  <?php

  // Create a connection.
   $db = null;
   if (isset($_SERVER['SERVER_SOFTWARE']) &&
      strpos($_SERVER['SERVER_SOFTWARE'], 'Google App Engine') !== false) {
    // Connect from App Engine.
	try {
		$db = new pdo('mysql:unix_socket=/cloudsql/test2-3-331:mydb;dbname=sotuken;charset=utf8', '3bji1113', '');
    	} catch (PDOException $ex) {
      		die('Unable to connect.');
    	}
   }
  ?>

 <h2>個人情報出力</h2>
    <form action="/phpselect">
      <div>
	<input type="submit" value="メニューへ戻る">
      </div>
    </form>
 <div>カラム指定なし</div>
  <form>
   <div><input type="submit" name="run" value="実行"></div>
  </form>
  <?php
	if(isset($_GET["run"])){
		echo "<br> カラム指定なし(1000件) <br>";
		for($i = 0; $i < 3; $i++){
	      	 	$stmt = $db->prepare('select * from Japanese order by rand() limit 1000');
			for($j = 1; $j < 11; $j++){
        			$start = microtime(true);
        			$stmt->execute();
       				$end = microtime(true);
        			$time = $end - $start;
        			echo "<br>$time";
        			$affected_rows = $stmt->rowCount();
       			}
		}
		echo "<br>";


		echo "<br> カラム指定なし(10000件) <br>";
		for($i = 0; $i < 3; $i++){
			$stmt = $db->prepare('select * from Japanese order by rand() limit 10000');
			for($j = 1; $j < 11; $j++){
        			$start = microtime(true);
        			$stmt->execute();
       				$end = microtime(true);
        			$time = $end - $start;
        			echo "<br>$time";
        			$affected_rows = $stmt->rowCount();
       			}
		}
		echo "<br>";


		echo "<br> カラム１指定int(1000件) <br>";
		for($i = 0; $i < 3; $i++){
			$stmt = $db->prepare('select ID from Japanese order by rand() limit 1000');	
			for($j = 1; $j < 11; $j++){
				$start = microtime(true);
    				$stmt->execute();
      				$end = microtime(true);
      				$time = $end - $start;
				echo "<br>$time";
      				$affected_rows = $stmt->rowCount();
			}
		}
		echo "<br>";


		echo "<br> カラム１指定int(10000件) <br>";
		for($i = 0; $i < 3; $i++){
			$stmt = $db->prepare('select ID from Japanese order by rand() limit 10000');	
			for($j = 1; $j < 11; $j++){
				$start = microtime(true);
    				$stmt->execute();
      				$end = microtime(true);
      				$time = $end - $start;
				echo "<br>$time";
      				$affected_rows = $stmt->rowCount();
			}
		}
		echo "<br>";


		echo "<br> カラム１指定char(1000件) <br>";
		for($i = 0; $i < 3; $i++){
			$stmt = $db->prepare('select LastName from Japanese order by rand() limit 1000');
                	for($j = 1; $j < 11; $j++){
                        	$start = microtime(true);
                        	$stmt->execute();
                        	$end = microtime(true);
                        	$time = $end - $start;
                        	echo "<br>$time";
                        	$affected_rows = $stmt->rowCount();
                	}
		}
		echo "<br>";


		echo "<br> カラム１指定char(10000件) <br>";
		for($i = 0; $i < 3; $i++){
			$stmt = $db->prepare('select LastName from Japanese order by rand() limit 10000');
                	for($j = 1; $j < 11; $j++){
                        	$start = microtime(true);
                        	$stmt->execute();
                        	$end = microtime(true);
                        	$time = $end - $start;
                        	echo "<br>$time";
                        	$affected_rows = $stmt->rowCount();
                	}
		}
		echo "<br>";


		echo "<br> カラム全部指定(1000件) <br>";
		for($i = 0; $i < 3; $i++){
        		$stmt = $db->prepare('select ID,LastName,FirstName,LastName_kana,FirstName_kana,LastName_roma,FirstName_roma,Age,Address from Japanese order by rand() limit 1000');
        		for($j = 1; $j < 11; $j++){
                		$start = microtime(true);
                		$stmt->execute();
                		$end = microtime(true);
                		$time = $end - $start;
                		echo "<br>$time";
                		$affected_rows = $stmt->rowCount();
        		}
		}
		echo "<br>";


		echo "<br> カラム全部指定(10000件) <br>";
		for($i = 0; $i < 3; $i++){
        		$stmt = $db->prepare('select ID,LastName,FirstName,LastName_kana,FirstName_kana,LastName_roma,FirstName_roma,Age,Address from Japanese order by rand() limit 10000');
        		for($j = 1; $j < 11; $j++){
                		$start = microtime(true);
                		$stmt->execute();
                		$end = microtime(true);
                		$time = $end - $start;
                		echo "<br>$time";
                		$affected_rows = $stmt->rowCount();
        		}
		}
  	}
?>

 </body>
</html>